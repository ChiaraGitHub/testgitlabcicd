FROM python:3.8-slim-buster

RUN pip install "poetry==1.1.13"

COPY main.py \
     README.md \
     pyproject.toml \
     poetry.lock ./

COPY app ./app

RUN poetry install --no-dev --no-interaction --no-ansi

CMD ["python", "main.py"]
